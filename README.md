# README

#### Ruby version
  `2.3.5`

#### Configuration

##### Development - Env

  Clone the project

```
git clone git@gitlab.com:Tihomirov/shippit.git
```

  Install libraries

```
bundle install
```

**Create .env.development**

And make sure you configure it

```
cp .env.production.sample .env.development
```

#### Database setup

**Run**

```
rake db:setup
```

**How to run the test suite**

Cucumber tests:

```
bundle exec cucumber
```

Rspec:

```
bundle exec rspec
```

#### Services (job queues, cache servers, search engines, etc.)

  The app uses sidekiq so Redis is mandatory

  Install Redis

*https://redis.io/download*

  You can also install this little runner to be able to have more hands-on experience

*https://gitlab.com/Tihomirov/shippit_api_server*
