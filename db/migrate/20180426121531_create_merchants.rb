class CreateMerchants < ActiveRecord::Migration[5.1]
  def change
    create_table :merchants do |t|
      t.string :name, null: false, unique: true
      t.string :phone_number, null: false
      t.string :api_token, null: false

      t.timestamps
    end
  end
end
