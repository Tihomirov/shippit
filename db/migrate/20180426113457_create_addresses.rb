class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.string :country, null: false
      t.string :city, null: false
      t.integer :postal_code, null: false
      t.string :street, null: false
      t.references :addressable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
