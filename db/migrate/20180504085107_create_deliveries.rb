class CreateDeliveries < ActiveRecord::Migration[5.1]
  def change
    create_table :deliveries do |t|
      t.references :order, foreign_key: true, null: false
      t.integer :track_id, null: false
      t.float :quote, null: false
      t.string :status
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
