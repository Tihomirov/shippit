class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.references :merchant, foreign_key: true, null: false
      t.string :recipient, null: false
      t.string :recipient_address, null: false
      t.string :courier
      t.string :shipment_type
      t.float :weight, null: false
      t.float :length, null: false
      t.float :width, null: false
      t.float :height, null: false
      t.datetime :confirmed_at

      t.timestamps
    end
  end
end
