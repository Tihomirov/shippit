Rails.application.routes.draw do
  devise_for :merchants, controllers: {
    passwords: 'merchants/passwords',
    registrations: 'merchants/registrations',
    sessions: 'merchants/sessions'
  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'merchants/orders#index'

  namespace :merchants do
    resource :orders, only: [:update]
    resources :orders, only: [] do
      put :confirm_order
      get :track
    end
  end

  get '/ready_to_ship' => 'merchants/orders#ready_to_ship'

  namespace :api do
    resource :orders, only: [:create] do
      post :geolocate
    end
  end
end
