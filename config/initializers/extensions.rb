if Rails.env.production?
  Dir[Rails.root.join('lib/**/*.rb')].each { |f| require f }
end
