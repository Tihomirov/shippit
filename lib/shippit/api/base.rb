module Api
  class Base < StructOrder
    class InvalidCode < StandardError; end
    class InvalidResponse < StandardError; end
  end
end
