require 'net/http'
require 'uri'

module Api
  # Aramex api handler
  class Aramex < Base
    def initialize(order)
      @order = order
    end

    attr_reader :order

    def create_shipment
      request = send_request('create')
      params = order_params(request.body)

      order.create_delivery(params)
    end

    def track_shipment
      request = send_request('track')
      params = delivery_params(request.body)

      order.delivery.update!(params)
    end

    def send_request(type)
      uri = URI.parse(ENV['ARAMEX_URL'])
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = uri.scheme == 'https'
      request = Net::HTTP::Post.new(uri.request_uri)
      request['Content-Type'] = 'application/xml; charset=UTF-8'
      request.body = load_template(type)
      http.request(request)
    end

    def load_template(type)
      options = {
        create: {
          path: 'app/views/api/aramex/shipment_request.xml.erb',
          order: build_order
        },
        track: {
          path: 'app/views/api/aramex/tracking_request.xml.erb',
          order: order
        }
      }

      html = File.open(options[type.to_sym][:path]).read
      bind = binding
      bind.local_variable_set(:order, options[type.to_sym][:order])
      ERB.new(html).result(bind)
    end

    private

    def xml_to_hash(xml)
      Hash.from_xml(xml.delete("\n"))
    end

    def check_code!(params)
      code = params['AramexShipmentResponse']['Result']['Code']
      error_message = "The response code returned by Aramex is unexpected: \"#{code}\""
      raise Base::InvalidCode, error_message unless code == 'SUCCESS'
    end

    def check_response_schema(response, elements)
      error_message = 'The response schema is invalid'

      elements.each do |element|
        raise Base::InvalidResponse, error_message unless response.match(element)
      end
    end

    def order_params(response)
      schema = %w[AramexShipmentResponse Result TrackingID Quote]
      check_response_schema(response, schema)

      params = xml_to_hash(response)
      check_code!(params)

      {
        track_id: params['AramexShipmentResponse']['Result']['TrackingID'],
        quote: params['AramexShipmentResponse']['Result']['Quote']
      }
    end

    def delivery_params(response)
      schema = %w[AramexTrackingResponse Status Location latitude longitude]
      check_response_schema(response, schema)

      params = xml_to_hash(response)

      {
        status: params['AramexTrackingResponse']['Status'],
        latitude: params['AramexTrackingResponse']['Location']['latitude'],
        longitude: params['AramexTrackingResponse']['Location']['longitude']
      }
    end
  end
end
