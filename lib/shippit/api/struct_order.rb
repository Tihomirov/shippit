module Api
  class StructOrder
    Struct.new(
      'Order',
      :id,
      :created_at,
      :recipient,
      :recipient_address,
      :merchant_name,
      :merchant_address,
      :weight
    )

    private

    def build_order
      Struct::Order.new(
        order.id,
        order.created_at,
        order.recipient,
        order.recipient_address,
        order.merchant.name,
        extract_address(order.merchant.addresses.first),
        order.weight
      )
    end

    def extract_address(address)
      [
        address.country,
        address.city,
        address.postal_code,
        address.street
      ].join(', ')
    end
  end
end
