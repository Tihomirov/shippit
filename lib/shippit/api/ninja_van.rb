require 'net/ftp'

module Api
  # Ninja Van ftp handler
  class NinjaVan < Base
    def initialize(order)
      @order = order
    end

    attr_reader :order

    def create_shipment
      ftp = send_order_file
      check_code!(ftp.last_response_code)

      order.update(confirmed_at: DateTime.current)
    end

    def send_order_file
      csv = create_csv_order

      ftp = Net::FTP.new
      ftp.connect(ENV['NINJA_VAN_FTP_ADDRESS'], ENV['NINJA_VAN_PORT'])
      ftp.login(ENV['NINJA_VAN_FTP_LOGIN'], ENV['NINJA_VAN_FTP_PASSWORD'])
      ftp.passive = false
      ftp.put(csv)
      csv.close
      ftp.close

      ftp
    end

    def create_csv_order
      order_values = build_order.values.join(', ')

      file = Tempfile.new([order.id.to_s, '.csv'])
      file.binmode
      file.write(order_values)
      file.rewind
      file
    end

    private

    def check_code!(code)
      ftp_codes = (200...229).to_a
      error_message = "The code returned by Ninja Van is unexpected: \"#{code}\""
      raise Base::InvalidCode, error_message unless ftp_codes.include?(code.to_i)
    end
  end
end
