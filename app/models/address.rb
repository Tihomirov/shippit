class Address < ApplicationRecord
  belongs_to :addressable, polymorphic: true, optional: true

  validates_presence_of :country, :city, :postal_code, :street
end
