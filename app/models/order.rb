class Order < ApplicationRecord
  TYPES = ['Parcel', 'Satchel 1kg'].freeze
  COURIERS = ['Aramex', 'Ninja Van'].freeze

  belongs_to :merchant
  has_one :delivery, dependent: :destroy

  validates_presence_of :recipient, :recipient_address, :weight, :length, :width, :height
  validates :shipment_type,
    inclusion: { in: TYPES,
    message: "\"%{value}\" is not a valid shipment_type" },
    allow_nil: true,
    allow_blank: true
  validates :courier,
    inclusion: { in: COURIERS,
    message: "\"%{value}\" is not a valid courier" },
    allow_nil: true,
    allow_blank: true

  def has_valid_fields?
    validate_shipment_type
    validate_courier

    !self.errors.any?
  end

  private

  def validate_shipment_type
    self.errors.add(:base, 'Shipment Type is invalid') unless TYPES.include?(shipment_type)
  end

  def validate_courier
    self.errors.add(:base, 'Courier is invalid') unless COURIERS.include?(courier)
  end
end
