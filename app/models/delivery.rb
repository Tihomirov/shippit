class Delivery < ApplicationRecord
  belongs_to :order

  validates :track_id, :quote, presence: true, on: :create
  validates :status, :latitude, :longitude, presence: true, on: :update

  after_save :confirm_order

  private

  def confirm_order
    order.update(confirmed_at: DateTime.current) unless order.confirmed_at?
  end
end
