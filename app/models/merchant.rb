class Merchant < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :addresses, as: :addressable, dependent: :destroy
  has_many :orders, dependent: :destroy
  before_create :generate_api_token
  accepts_nested_attributes_for :addresses, allow_destroy: true

  validates :name, uniqueness: true
  validates :name, :phone_number, presence: true

  private

  def generate_api_token
    self.api_token = SecureRandom.hex(16).to_i(16).to_s(36)
  end
end
