class OrderCreationWorker
  include Sidekiq::Worker

  def perform(merchant_id, params)
    merchant = Merchant.find(merchant_id)
    params = JSON.parse(params)
    merchant.orders.create!(params)
  rescue ActiveRecord::RecordInvalid => ex
    logger.warn ex
  end
end
