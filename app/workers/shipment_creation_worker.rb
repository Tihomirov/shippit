class ShipmentCreationWorker
  include Sidekiq::Worker
  PROVIDER_ERRORS = [
    Api::Base::InvalidCode,
    URI::InvalidURIError
  ].freeze

  def perform(order_id)
    order = Order.find(order_id)

    courier_api(order.courier).new(order).create_shipment
  rescue *PROVIDER_ERRORS => ex
    logger.warn ex
  end

  private

  def courier_api(courier)
    courier = courier.delete(' ').camelcase
    "Api::#{courier}".classify.constantize
  end
end
