class TrackShipmentWorker
  include Sidekiq::Worker

  def perform(order_id)
    order = Order.find(order_id)

    courier_api(order.courier).new(order).track_shipment
  rescue ActiveRecord::RecordInvalid, Api::Base::InvalidResponse => ex
    logger.warn ex
  end

  private

  def courier_api(courier)
    courier = courier.delete(' ').camelcase
    "Api::#{courier}".classify.constantize
  end
end
