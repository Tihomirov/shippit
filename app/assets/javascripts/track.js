$(document).ready(function () {
  var map = $("#map"),
      token = $('meta[name="csrf-token"]').attr('content');

  if(map.length > 0) {
    var orderId = window.location.pathname.split('/')[3],
        params = {order: { id: orderId } };

    $.ajax({
      method: 'POST',
      timeout: 30000,
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-Token', token)
      },
      url: '/api/orders/geolocate',
      data: params,
      success: function(data) {
        initMap(data);
        shipmentInfo(data);
      },
      error: function(data) {
        console.log('Unable To Retrieve Data');
      }
    });
  }

  function shipmentInfo(data) {
    var info = $('#shipment-info'),
        message = $('.shipment .message'),
        trackId = `<p>Tracking ID: ${data['track_id']}</p>`,
        status = `<p>Status: ${data['status']}</p>`;

    if(data['status'] === null || data['status'] === undefined) {
      setTimeout(function() {
        location.reload()
      }, 2500);
    }

    info.html(trackId + status);
    message.hide();
  };
});

var initMap = function(data) {
  if(typeof data === 'undefined' || typeof google === 'undefined') {
    return;
  }

  var latLng = {lat: data['latitude'], lng: data['longitude']};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 16,
    center: latLng
  });

  var cityCircle = new google.maps.Circle({
      strokeColor: '#87b8c9',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#87b8c9',
      fillOpacity: 0.35,
      map: map,
      center: latLng,
      radius: 100
    });

  infoWindow = new google.maps.InfoWindow;
};
