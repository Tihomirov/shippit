$(document).ready(function() {
  var selects = $('.order-list td select'),
      token = $('meta[name="csrf-token"]').attr('content');

  selects.on('change', function() {
    progressBar('show');
    var orderInfo = $(this).parents('tr').data("order"),
        selectValue = $(this).children('option:selected').data("shipment"),
        orderParams = { order: $.extend({}, orderInfo, selectValue)};

    $.ajax({
      method: 'PUT',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-Token', token)
      },
      url: '/merchants/orders',
      data: orderParams,
      success: function(data) {
        progressBar();
      }
    });
  });

  function progressBar(action) {
    if(action === 'show') {
      Turbolinks.controller.adapter.progressBar.setValue(0);
      Turbolinks.controller.adapter.progressBar.show();
    } else {
      Turbolinks.controller.adapter.progressBar.hide();
      Turbolinks.controller.adapter.progressBar.setValue(100);
    }
  };
});
