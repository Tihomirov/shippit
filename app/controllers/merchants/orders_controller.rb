class Merchants::OrdersController < ApplicationController
  before_action :authenticate_merchant!
  before_action :set_order, only: :update

  def index
    @orders = current_merchant.orders.where('confirmed_at IS NULL')
                              .order('created_at DESC')
  end

  def ready_to_ship
    @orders = current_merchant.orders.where('confirmed_at NOT NULL')
                              .order('confirmed_at')
  end

  def track
  end

  def update
    respond_to do |format|
      if @order.update(order_params)
        format.js
      else
        format.js { render json: @order.errors.messages.to_json, status: :bad_request }
      end
    end
  end

  def confirm_order
    order = current_merchant.orders.find(params[:order_id])

    if params[:confirm] && order.has_valid_fields?
      ShipmentCreationWorker.perform_async(order.id)
      redirect_to root_path, notice: 'Order confirmation has started.'
    else
      redirect_to root_path, alert: order.errors.messages[:base].join(', ')
    end
  end

  private

  def set_order
    @order = current_merchant.orders.find(order_params[:id])
  end

  def order_params
    params.fetch(:order).permit(:id, :shipment_type, :courier)
  end
end
