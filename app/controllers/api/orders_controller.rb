class Api::OrdersController < ApplicationController
  protect_from_forgery unless: -> { request.format.json? }
  before_action :set_merchant, only: :create

  def create
    respond_to do |format|
      if @merchant
        OrderCreationWorker.perform_async(@merchant.id, order_params.to_json)
        format.json { render json: { status: '201 Order Accepted' }.to_json, status: :created }
      else
        format.json { render json: { status: '404 Not Found' }.to_json, status: :not_found }
      end
    end
  end

  def geolocate
    TrackShipmentWorker.perform_async(order_params[:id])
    order = Order.find(order_params[:id])

    respond_to do |format|
      if order.delivery
        format.json { render json: order.delivery.to_json, status: :accepted }
      else
        format.json { render json: { status: 'Failed to load' }.to_json, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_merchant
    @merchant = Merchant.find_by(api_token: request.headers['HTTP_API_TOKEN'])
  end

  def order_params
    params.fetch(:order).permit(%i[id recipient recipient_address weight length width height])
  end
end
