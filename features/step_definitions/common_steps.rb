Given(/^I am looking at "([^"]*)"$/) do |path|
  visit path
end

When(/^I click "([^"]*)" button$/) do |name|
  click_button(name)
end

When(/^I click "([^"]*)" link$/) do |name|
  click_link(name)
end

Then(/^I should be redirected to "([^"]*)"$/) do |path|
  expect(page).to have_current_path(path, ignore_query: true)
end

Then(/^I should see "([^"]*)" within "([^"]*)"$/) do |value, selector|
  element = find(selector)
  expect(element.text).to eq(value)
end
