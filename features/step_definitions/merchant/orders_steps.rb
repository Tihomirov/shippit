Given(/^I have (\d+) ord(?:er|ers) with shipment_type "([^"]*)" and courier "([^"]*)"$/) do |number, shipment_type, courier|
  create_list(
    :order,
    number.to_i,
    shipment_type: shipment_type,
    courier: courier,
    confirmed_at: nil,
    merchant_id: @merchant.id
  )
end

Given(/^I have (\d+) confirmed orders$/) do |orders_count|
  create_list(:orders_with_deliveries, orders_count.to_i, merchant_id: @merchant.id)
end

When(/^I log in$/) do
  steps %{
    And I am looking at "/merchants/sign_in"
    When I fill in my account credentials
    And I click "Log in" button
    Then I should be redirected to "/"
  }
end

When(/^I change the "([^"]*)" to "([^"]*)"$/) do |selector, order_type|
  first(".order-list table tr.order #{selector}")
    .find(:option, order_type).select_option
end

When(/^I click the confirm button for the order with courier "([^"]*)"$/) do |courier_name|
  script = "
    $('option[value=\"#{courier_name}\"]:selected')
      .parents('tr').find('input[type=\"submit\"]').click()"
  page.execute_script(script)
end

When(/^I click the confirm button for the first order$/) do
  within('.order-list table') do
    first('input[type="submit"]').click
  end
end

When(/^I click "([^"]*)" button for the first order$/) do |button_name|
  first('tr.order').find("input[value='#{button_name}']").click
end

Then(/^I should see that i have (\d+) orders$/) do |orders_count|
  within('.order-list table') do
    orders = all('tr').count - 2
    expect(orders).to eq(orders_count.to_i)
  end
end

Then(/^The "([^"]*)" field should be updated to "([^"]*)"$/) do |field, order_type|
  count = @merchant.orders.where("#{field} = '#{order_type}'").count
  expect(count).to eq(1)
end

Then(/^I should see a map for the shipment$/) do
  within('#map') do
    gm_element = find('.gmnoprint:first-of-type')
    expect(gm_element.present?).to be true
  end
end
