Given(/^I have a merchant account$/) do
  @merchant = create(:merchant)
end

When(/^I fill in my account credentials$/) do
  within('.log-in form') do
    fill_in 'merchant_email', with: @merchant.email
    fill_in 'merchant_password', with: @merchant.password
  end
end
