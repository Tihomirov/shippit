When(/^I fill in the registration form with valid data$/) do
  within('#new_merchant') do
    fill_in 'merchant_name', with: 'foobar-computers'
    fill_in 'merchant_phone_number', with: '+359 899 999999'
    fill_in 'merchant_email', with: 'foobar-computers@foobar.com'
    fill_in 'merchant_password', with: 'password'
    fill_in 'merchant_password_confirmation', with: 'password'
    fill_in 'merchant_addresses_attributes__country', with: 'Bulgaria'
    fill_in 'merchant_addresses_attributes__city', with: 'Sofia'
    fill_in 'merchant_addresses_attributes__postal_code', with: '1000'
    fill_in 'merchant_addresses_attributes__street', with: 'Alabin 10'
  end
end
