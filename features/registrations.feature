Feature: Registration

Scenario: Valid registration data creates a new merchant
  Given I am looking at "/merchants/sign_up"
  When I fill in the registration form with valid data
  And I click "Sign up" button
  Then I should be redirected to "/"
  And I should see "Welcome! You have signed up successfully." within ".notice"
