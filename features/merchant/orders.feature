Feature: Merchant Orders

Scenario: Home path show all my orders
  Given I have a merchant account
  And I have 3 orders with shipment_type "" and courier ""
  When I log in
  Then I should see that i have 3 orders

@javascript
@logged_in
Scenario: I am able to change the order type
  Given I have 2 orders with shipment_type "" and courier ""
  And I am looking at "/"
  When I change the ".shipment_type" to "Satchel 1kg"
  Then The "shipment_type" field should be updated to "Satchel 1kg"

@javascript
@logged_in
Scenario: I am able to change the courier type
  Given I have 5 orders with shipment_type "" and courier ""
  And I am looking at "/"
  When I change the ".courier_type" to "Aramex"
  Then The "courier" field should be updated to "Aramex"

@javascript
@logged_in
Scenario: I am able to confirm an order
  Given I have 1 order with shipment_type "Parcel" and courier "Aramex"
  And I have 2 orders with shipment_type "" and courier ""
  And I am looking at "/"
  When I click the confirm button for the order with courier "Aramex"
  Then I should see "Order confirmation has started." within ".notice"

@javascript
@logged_in
Scenario: I recieve and error if i confirm an order with --select-type--
  And I have 3 orders with shipment_type "" and courier "Aramex"
  And I am looking at "/"
  When I click the confirm button for the first order
  Then I should see "Shipment Type is invalid" within ".alert"

@javascript
@logged_in
Scenario: I recieve and error if i confirm an order with --select-courier--
  And I have 3 orders with shipment_type "Parcel" and courier ""
  And I am looking at "/"
  When I click the confirm button for the first order
  Then I should see "Courier is invalid" within ".alert"

@javascript
@logged_in
Scenario: I am able to view my confirmed orders
  Given I have 3 confirmed orders
  And I am looking at "/"
  When I click "Ready to Ship" link
  Then I should be redirected to "/ready_to_ship"
  And I should see that i have 3 orders

@javascript
@logged_in
Scenario: I am able to track a confirmed order
  Given I have 3 confirmed orders
  And I am looking at "/ready_to_ship"
  When I click "Track Order" button for the first order
  Then I should see a map for the shipment
