Feature: Login

Scenario: Merchant login
  Given I have a merchant account
  And I am looking at "/merchants/sign_in"
  When I fill in my account credentials
  And I click "Log in" button
  Then I should be redirected to "/"
  And I should see "Signed in successfully." within ".notice"
