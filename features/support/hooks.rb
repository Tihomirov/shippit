Before('@logged_in') do
  @merchant = create(:merchant)
  visit '/merchants/sign_in'
  fill_in "merchant_email", :with => @merchant.email
  fill_in "merchant_password", :with => @merchant.password
  click_button "Log in"
end
