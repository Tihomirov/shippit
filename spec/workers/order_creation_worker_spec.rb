require 'rails_helper'

RSpec.describe OrderCreationWorker do
  describe '#perform' do
    let(:merchant) { create(:merchant) }
    let(:params) do
      {
        recipient: 'Richard',
        recipient_address: 'USA, Chicago, St.Patrick 15',
        weight: 15,
        length: 20,
        width: 40,
        height: 30
      }
    end

    it 'creates a new order for a given merchant if params are valid' do
      worker = described_class.new

      expect do
        worker.perform(merchant.id, params.to_json)
      end.to change { Order.count }.by(1)
    end

    it 'prompts a warning to the sidekiq logger if params are invalid' do
      params[:weight] = ''

      expect(Sidekiq.logger).to receive(:warn)

      worker = described_class.new
      worker.perform(merchant.id, params.to_json)
    end
  end
end
