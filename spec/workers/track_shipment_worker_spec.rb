require 'rails_helper'

RSpec.describe TrackShipmentWorker do
  describe '#perform' do
    let(:delivery) { create(:delivery) }
    before do
      ENV['ARAMEX_URL'] = 'http://b2b-uat-farm.aramex.com.au'
    end

    it 'updates a delivery for a given order' do
      factory = AramexResponsesFactory.build_tracking_response(delivery.track_id)

      stub_request(:post, ENV['ARAMEX_URL'])
        .with(
          body: /(...)/,
          headers: {
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Content-Type' => 'application/xml; charset=UTF-8',
            'User-Agent' => 'Ruby'
          }
        ).to_return(body: factory)

      res_hash = Hash.from_xml(factory.delete("\n"))['AramexTrackingResponse']
      described_class.new.perform(delivery.order.id)
      delivery.reload

      expect(delivery.track_id).to eq(res_hash['TrackingID'].to_i)
      expect(delivery.status).to eq(res_hash['Status'])
      expect(delivery.latitude).to eq(res_hash['Location']['latitude'].to_f)
      expect(delivery.longitude).to eq(res_hash['Location']['longitude'].to_f)
    end

    it 'prompts a warning to the sidekiq logger if params are invalid' do
      order = create(:delivery).order
      stub_request(:post, ENV['ARAMEX_URL'])
        .with(
          body: /(...)/,
          headers: {
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Content-Type' => 'application/xml; charset=UTF-8',
            'User-Agent' => 'Ruby'
          }
        ).to_return(body: '')

      expect(Sidekiq.logger).to receive(:warn)

      worker = described_class.new
      worker.perform(order.id)
    end
  end
end
