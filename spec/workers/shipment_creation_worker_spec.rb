require 'rails_helper'

RSpec.describe ShipmentCreationWorker do
  Struct.new('Ftp', :last_response_code)

  describe '#perform' do
    context 'places a new shipment based on the order courier' do
      it 'updates order confirmed_at and creates a delivery on success Aramex' do
        order = create(:order, confirmed_at: nil)
        ENV['ARAMEX_URL'] = 'http://b2b-uat-farm.aramex.com.au'
        response_factory = AramexResponsesFactory.build_shipment_response(order.id, track_id: 1, quote: 25)

        stub_request(:post, ENV['ARAMEX_URL'])
          .with(
            body: /(...)/,
            headers: {
              'Accept' => '*/*',
              'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
              'Content-Type' => 'application/xml; charset=UTF-8',
              'User-Agent' => 'Ruby'
            }
          ).to_return(body: response_factory)

        worker = ShipmentCreationWorker.new
        worker.perform(order.id)
        order.reload

        expect(order.confirmed_at?).to be true
        expect(order.delivery.track_id).to eq(order.id)
        expect(order.delivery.quote).to eq(25)
      end

      it 'updates the order to confirmed on success for NinjaVan courier' do
        order = create(:order, courier: 'Ninja Van', confirmed_at: nil)
        allow_any_instance_of(Api::NinjaVan).to receive(:send_order_file)
          .and_return(Struct::Ftp.new(226))

        worker = ShipmentCreationWorker.new
        worker.perform(order.id)
        order.reload

        expect(order.confirmed_at?).to be true
        expect(order.delivery).to be nil
      end

      it 'prompts a warning to the sidekiq logger if a response is invalid' do
        order = create(:order, courier: 'Ninja Van', confirmed_at: nil)
        allow_any_instance_of(Api::NinjaVan).to receive(:send_order_file)
          .and_return(Struct::Ftp.new(400))

        expect(Sidekiq.logger).to receive(:warn)

        worker = ShipmentCreationWorker.new
        worker.perform(order.id)
      end
    end
  end
end
