require 'rails_helper'

RSpec.describe Delivery, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:track_id) }
    it { is_expected.to validate_presence_of(:quote) }
    it { is_expected.to validate_presence_of(:status).on(:update) }
    it { is_expected.to validate_presence_of(:latitude).on(:update) }
    it { is_expected.to validate_presence_of(:longitude).on(:update) }
  end
end
