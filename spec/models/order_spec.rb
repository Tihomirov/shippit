require 'rails_helper'

RSpec.describe Order, type: :model do
  describe 'standart validations' do
    it { is_expected.to validate_presence_of(:recipient) }
    it { is_expected.to validate_presence_of(:recipient_address) }
    it { is_expected.to validate_presence_of(:weight) }
    it { is_expected.to validate_presence_of(:length) }
    it { is_expected.to validate_presence_of(:width) }
    it { is_expected.to validate_presence_of(:height) }
    it do
      is_expected.to validate_inclusion_of(:shipment_type)
        .in_array(['Parcel', 'Satchel 1kg'])
    end
    it do
      is_expected.to validate_inclusion_of(:courier)
        .in_array(['Aramex', 'Ninja Van'])
    end
  end

  describe 'has_valid_fields?' do
    context 'custom validates the order shipment_type and courier' do
      it 'passes if shipment_type and courier are valid' do
        order = create(:order, confirmed_at: nil)

        expect(order.has_valid_fields?).to be true
      end

      it 'fails if shipment_type is empty/invalid' do
        order = create(:order, shipment_type: nil, confirmed_at: nil)

        expect(order.has_valid_fields?).to be false
        expect(order.errors.messages[:base]).to eq(['Shipment Type is invalid'])
      end

      it 'fails if courier is empty/invalid' do
        order = create(:order, courier: nil)

        expect(order.has_valid_fields?).to be false
        expect(order.errors.messages[:base]).to eq(['Courier is invalid'])
      end
    end
  end
end
