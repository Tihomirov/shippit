require 'rails_helper'

RSpec.describe OrderHelper, type: :helper do
  describe 'select_options_tag' do
    it 'creates select tag and adds selected based on passed value' do
      order = create(:order, shipment_type: 'Satchel 1kg')
      args = [['Satchel 1kg', 'Satchel 1kg'], ['1']]

      result = select_options_tag(nil, args, value: order.shipment_type)

      expect(result).to match(/<option selected=\"selected\" value=\"Satchel 1kg\"/)
    end
  end
end
