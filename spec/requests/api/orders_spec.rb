require 'rails_helper'

RSpec.describe '/api/orders', type: :request do
  describe 'create' do
    let(:merchant) { create(:merchant) }
    let(:params) do
      {
        recipient: 'Richard',
        recipient_address: 'USA, Chicago, St.Patrick 15',
        weight: 15,
        length: 20,
        width: 40,
        height: 30
      }
    end

    it 'starts a order creation worker if the token is valid' do
      headers = {
        'HTTP_API_TOKEN' => merchant.api_token,
        'ACCEPT' => 'application/json',
        'CONTENT_TYPE' => 'application/json'
      }

      post '/api/orders', params: params.to_json, headers: headers

      expect(OrderCreationWorker.jobs[0]['args']).to eq([merchant.id, params.to_json])
      expect(response).to have_http_status(:created)
    end

    it 'returns :not_found if token is invalid' do
      invalid_token_header = {
        'HTTP_API_TOKEN' => 'invalid_token',
        'ACCEPT' => 'application/json',
        'CONTENT_TYPE' => 'application/json'
      }
      post '/api/orders', params: params.to_json, headers: invalid_token_header

      expect(OrderCreationWorker.jobs).to be_empty
      expect(response).to have_http_status(:not_found)
    end
  end
end
