require 'rails_helper'

RSpec.describe Merchants::OrdersController, type: :controller do
  include Devise::Test::ControllerHelpers

  before do
    @request.env['devise.mapping'] = Devise.mappings[:merchant]
    sign_in merchant
  end

  describe 'PUT update' do
    let(:merchant) { create(:merchant) }
    let(:order) { create(:order, shipment_type: nil, courier: nil, merchant: merchant) }

    context 'shipment_type param' do
      it 'sets the shipment_type' do
        params = {
          "order"=> {
            "id"=> order.id,
            "shipment_type"=>"Satchel 1kg"
          }
        }

        put :update, xhr: true, params: params

        expect(response.code.to_i).to eq(204)
      end
    end

    context 'courier param' do
      it 'sets the courier' do
        params = {
          "order"=> {
            "id"=> order.id,
            "courier"=>"Aramex"
          }
        }

        put :update, xhr: true, params: params

        expect(response.code.to_i).to eq(204)
      end
    end

    context 'invalid' do
      it 'fails if the courier param is invalid' do
        params = {
          "order"=> {
            "id"=> order.id,
            "courier"=>"invalid"
          }
        }
        expected_message = {
          "courier" => [
            "\"#{params["order"]["courier"]}\" is not a valid courier"
          ]
        }

        put :update, xhr: true, params: params

        expect(response.code.to_i).to eq(400)
        expect(response.body).to eq(expected_message.to_json)
      end

      it 'fails if the shipment_type param is invalid' do
        params = {
          "order"=> {
            "id"=> order.id,
            "shipment_type"=>"invalid"
          }
        }
        expected_message = {
         "shipment_type" => [
            "\"#{params["order"]["shipment_type"]}\" is not a valid shipment_type"
          ]
        }

        put :update, xhr: true, params: params

        expect(response.code.to_i).to eq(400)
        expect(response.body).to eq(expected_message.to_json)
      end
    end
  end

  describe 'PUT confirm_order' do
    let(:merchant) { create(:merchant) }
    let(:order) { create(:order, confirmed_at: nil, merchant: merchant) }

    it 'starts a sidekiq worker to confirm the order if the order is valid' do
      params = {
        "order_id" => order.id,
        "confirm" => "true"
      }

      put :confirm_order, params: params

      expect(response.code.to_i).to eq(302)
      expect(ShipmentCreationWorker.jobs.count).to eq(1)
    end
  end
end
