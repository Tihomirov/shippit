require 'rails_helper'

RSpec.describe Api::Aramex do
  let(:order) { create(:order, confirmed_at: nil) }

  describe '.create_shipment' do
    before { ENV['ARAMEX_URL'] = 'http://b2b-uat-farm.aramex.com.au' }

    it 'updates the order if the request response is valid' do
      response_factory = AramexResponsesFactory.build_shipment_response(order.id, track_id: 1, quote: 2)

      stub_request(:post, ENV['ARAMEX_URL'])
        .with(
          body: /(...)/,
          headers: {
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Content-Type' => 'application/xml; charset=UTF-8',
            'User-Agent' => 'Ruby'
          }
        ).to_return(body: response_factory)

      res_hash = Hash.from_xml(response_factory.delete("\n"))['AramexShipmentResponse']
      aramex = described_class.new(order)

      aramex.create_shipment

      expect(order.delivery.track_id).to eq(res_hash['Result']['TrackingID'].to_i)
      expect(order.delivery.track_id).to eq(res_hash['Result']['TrackingID'].to_i)
      expect(order.delivery.quote).to eq(res_hash['Result']['Quote'].delete('$').to_f)
      expect(order.confirmed_at?).to be true
    end

    it 'raises an error if the returned body code is not successful' do
      factory = AramexResponsesFactory.build_shipment_response(order.id, code: 'invalid')
      stub_request(:post, ENV['ARAMEX_URL'])
        .with(
          body: /(...)/,
          headers: {
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Content-Type' => 'application/xml; charset=UTF-8',
            'User-Agent' => 'Ruby'
          }
        ).to_return(body: factory)

      aramex = described_class.new(order)

      expect do
        aramex.create_shipment
      end.to raise_error('The response code returned by Aramex is unexpected: "invalid"')
    end
  end

  describe '.track_shipment' do
    it 'updates an order\'s delivery if response is valid' do
      ENV['ARAMEX_URL'] = 'http://b2b-uat-farm.aramex.com.au'
      delivery = create(:delivery, status: nil, latitude: nil, longitude: nil)
      factory = AramexResponsesFactory.build_tracking_response(delivery.track_id)

      stub_request(:post, ENV['ARAMEX_URL'])
        .with(
          body: /(...)/,
          headers: {
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Content-Type' => 'application/xml; charset=UTF-8',
            'User-Agent' => 'Ruby'
          }
        ).to_return(body: factory)

      res_hash = Hash.from_xml(factory.delete("\n"))['AramexTrackingResponse']
      aramex = described_class.new(delivery.order)
      aramex.track_shipment

      expect(delivery.track_id).to eq(res_hash['TrackingID'].to_i)
      expect(delivery.status).to eq(res_hash['Status'])
      expect(delivery.latitude).to eq(res_hash['Location']['latitude'].to_f)
      expect(delivery.longitude).to eq(res_hash['Location']['longitude'].to_f)
    end
  end

  describe '.send_request' do
    before do
      @original_aramex_url = ENV['ARAMEX_URL']
    end

    after do
      ENV['ARAMEX_URL'] = @original_aramex_url
    end

    it 'sends a create shipment request to aramex' do
      ENV['ARAMEX_URL'] = 'http://b2b-uat-farm.aramex.com.au'

      response_factory = AramexResponsesFactory.build_shipment_response(order.id)

      stub_request(:post, ENV['ARAMEX_URL'])
        .with(
          body: /(...)/,
          headers: {
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Content-Type' => 'application/xml; charset=UTF-8',
            'User-Agent' => 'Ruby'
          }
        ).to_return(body: response_factory)

      aramex = described_class.new(order)
      response = aramex.send_request('create')

      expect(response.body).to eq(response_factory)
    end

    it 'sends a track shipment request to aramex' do
      ENV['ARAMEX_URL'] = 'http://b2b-uat-farm.aramex.com.au'
      delivery = create(:delivery)
      aramex = described_class.new(delivery.order)
      response_factory = AramexResponsesFactory.build_tracking_response(delivery.track_id)

      stub_request(:post, ENV['ARAMEX_URL'])
        .with(
          body: /(...)/,
          headers: {
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Content-Type' => 'application/xml; charset=UTF-8',
            'User-Agent' => 'Ruby'
          }
        ).to_return(body: response_factory)

      response = aramex.send_request('track')

      expect(response.body).to eq(response_factory)
    end
  end

  describe '.load_request_template' do
    context 'loads a template base on parameter' do
      it 'returns a create request template with order data' do
        aramex = described_class.new(order)
        struct_order = aramex.send(:build_order)
        request_factory = AramexResponsesFactory.build_shipment_request(struct_order)

        result = aramex.load_template('create')

        expect(result.delete(' ')).to eq(request_factory.delete(' '))
      end

      it 'returns a tracking request template with order data' do
        delivery = create(:delivery)
        aramex = described_class.new(delivery.order)
        request_factory = AramexResponsesFactory.build_tracking_request(delivery)

        result = aramex.load_template('track')

        expect(result.delete(' ')).to eq(request_factory.delete(' '))
      end
    end
  end
end
