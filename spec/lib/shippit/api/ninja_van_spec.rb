require 'rails_helper'
require 'fake_ftp'

RSpec.describe Api::NinjaVan do
  let(:order) { create(:order, confirmed_at: nil) }

  describe '.create_shipment' do
    Struct.new('Ftp', :last_response_code)

    it 'sets order confirmed_at if NinjaVan returns any of the successful codes' do
      allow_any_instance_of(described_class).to receive(:send_order_file)
        .and_return(Struct::Ftp.new(226))

      described_class.new(order).create_shipment

      expect(order.confirmed_at?).to be true
    end

    it 'raises an error if the status code is out of scope' do
      allow_any_instance_of(described_class).to receive(:send_order_file)
        .and_return(Struct::Ftp.new(400))

      expect do
        described_class.new(order).create_shipment
      end.to raise_error('The code returned by Ninja Van is unexpected: "400"')
    end
  end

  describe '.send_order_file' do
    it 'sends an order shipment file to NinjaVan' do
      ENV['NINJA_VAN_FTP_ADDRESS'] = '127.0.0.1'
      ENV['NINJA_VAN_PORT'] = '21212'
      ENV['NINJA_VAN_FTP_LOGIN'] = 'user'
      ENV['NINJA_VAN_FTP_PASSWORD'] = 'password'

      server = FakeFtp::Server.new(ENV['NINJA_VAN_PORT'].to_i)
      server.start

      ninja_van = described_class.new(order)
      ninja_van.send_order_file

      expect(server.files.count).to eq(1)
      expect(server.files[0]).to match(/.csv$/)
    end
  end

  describe '.create_csv_order' do
    it 'creates a .csv tempfile for ftp upload' do
      ninja_van = described_class.new(order)
      file = ninja_van.create_csv_order

      expect(file.class.to_s).to eq('Tempfile')
      expect(file.path).to match(/.csv$/)
    end
  end
end
