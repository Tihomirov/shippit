class AramexResponsesFactory
  def self.build_shipment_request(order)
    <<-EOS
    <AramexShipmentRequest>
      <Header>
        <RequestID>#{order.id}</RequestID>
        <DateTimeStamp>#{order.created_at}</DateTimeStamp>
      </Header>
      <Shipment>
        <RecipientName>#{order.recipient}</RecipientName>
        <RecipientAddress>#{order.recipient_address}</RecipientAddress>
        <MerchantCompanyName>#{order.merchant_name}</MerchantCompanyName>
        <MerchantAddress>#{order.merchant_address}</MerchantAddress>
        <ParcelWeight>#{order.weight}</ParcelWeight>
      </Shipment>
    </AramexShipmentRequest>
    EOS
  end

  def self.build_shipment_response(request_id, options = {})
    <<-EOS
    <AramexShipmentResponse>
      <Header>
        <RequestID>#{request_id}</RequestID>
      </Header>
      <Result>
        <Code>#{options[:code] || 'SUCCESS'}</Code>
        <TrackingID>#{options[:track_id]}</TrackingID>
        <Quote>#{options[:quote].to_s + '$' if options[:quote]}</Quote>
      </Result>
    </AramexShipmentResponse>
    EOS
  end

  def self.build_tracking_request(delivery)
    <<-EOS
    <AramexTrackingRequest>
      <TrackingID>#{delivery.track_id}</TrackingID>
      <DateTimeStamp>#{delivery.order.confirmed_at}</DateTimeStamp>
    </AramexTrackingRequest>
    EOS
  end

  def self.build_tracking_response(track_id)
    <<-EOS
    <AramexTrackingResponse>
      <TrackingID>#{track_id}</TrackingID>
      <Status>IN_TRANSIT</Status>
      <Location>
        <latitude>#{rand(150_999_999) / 100_000_0.0}</latitude>
        <longitude>#{rand(150_999_999) / 100_000_0.0}</longitude>
      </Location>
    </AramexTrackingResponse>
    EOS
  end
end
