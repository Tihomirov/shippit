FactoryBot.define do
  factory :merchant do
    sequence(:name) { |n| "Store #{n}" }
    sequence(:phone_number) { "+ 359 89#{rand(9999999)}" }
    sequence(:email) { |n| "store#{n}@example.com" }
    password "secureencryptedpassword"

    transient do
      addresses_count 1
    end

    after(:create) do |merchant, evaluator|
      create_list(:address, evaluator.addresses_count, addressable: merchant)
    end
  end
end
