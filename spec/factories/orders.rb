FactoryBot.define do
  factory :order do
    merchant
    sequence(:recipient) { |n| "Richard #{n}" }
    sequence(:recipient_address) { |n| "USA, Chicago, St.Peter #{n}" }
    courier 'Aramex'
    shipment_type 'Parcel'
    weight 5
    length 10.5
    width 20.5
    height 12.5
    confirmed_at 5.minutes.ago
    created_at 10.minutes.ago
    updated_at 10.minutes.ago

    factory :orders_with_deliveries do
      after(:create) do |order|
        create(:delivery, order: order)
      end
    end
  end
end
