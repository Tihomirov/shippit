FactoryBot.define do
  factory :address do
    country "Bulgaria"
    city "Sofia"
    postal_code 1000
    sequence(:street) { |n|  "Alabin #{n}" }
  end
end
