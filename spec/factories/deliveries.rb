FactoryBot.define do
  factory :delivery do
    order
    sequence(:track_id) { rand(9999) }
    sequence(:quote) { rand(55) }
    status 'SUCCESS'
    sequence(:latitude) { "#{rand(155)}.#{rand(999_999)}".to_f }
    sequence(:longitude) { "#{rand(155)}.#{rand(999_999)}".to_f }
  end
end
